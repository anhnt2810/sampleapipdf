﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Cors;

namespace SampleApiPdf.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DownloadFileController : ControllerBase
    {
        private IHostingEnvironment _env;
        public DownloadFileController(IHostingEnvironment env)
        {
            _env = env;
        }

        [HttpGet("[action]/{file_name}")]
        [DisableCors]
        public IActionResult DownloadPdf(string file_name)
        {
            string path = Path.Combine(_env.WebRootPath, "App_Data", file_name);
            byte[] all_byte = System.IO.File.ReadAllBytes(path);
            return File(all_byte, "application/octet-stream", file_name);
        }

    }
}